import "../css/main.css";
import * as signalR from "@aspnet/signalr";

const divMessages: HTMLDivElement = document.querySelector("#divMessages");
const tbMessage: HTMLInputElement = document.querySelector("#tbMessage");
const btnSend: HTMLButtonElement = document.querySelector("#btnSend");
const username = new Date().getTime();

const connection = new signalR.HubConnectionBuilder()
    .withUrl("/api/chat")
    .build();

connection.start().catch(err => document.write(err));


connection.on("messageReceived", (username: string, message: string) => {
    const m = document.createElement("div");
    m.innerHTML = `<div class="message-author">${username}</div><div>${message}</div>`;
    divMessages.appendChild(m);
    divMessages.scrollTop = divMessages.scrollHeight;
});


if (tbMessage) {
    tbMessage.addEventListener("keyup", (e: KeyboardEvent) => {
        if (e.keyCode === 13) {
            send();
        }
    });
}


if (btnSend) {
    btnSend.addEventListener("click", send);
}


function send() {
    connection.send("newMessage", username, tbMessage.value)
    .then(() => tbMessage.value = "");
}




const contractHub = new signalR.HubConnectionBuilder()
    .withUrl("/api/chat")
    .build();

contractHub.start().catch(err => document.write(err));

contractHub.on("actionStarted", (username: string, message: string) => {
    console.log("actionStarted");
    const m = document.createElement("div");
    m.innerHTML = `<div class="action-holder">${username}</div><div>${message}</div>`;
    document.querySelector("#action-holder").remove();
    document.querySelector("#actions").append(m);
});

var checkDiscountButton = document.querySelector("#checkDiscount");
console.log(checkDiscountButton);
if (checkDiscountButton) {
    console.log("click");
    checkDiscountButton.addEventListener("click", checkDiscount);
}


function checkDiscount() {
    console.log("checkDiscount");
    contractHub.send("newMessage", username, tbMessage.value)
        .then(() => tbMessage.value = "");
}