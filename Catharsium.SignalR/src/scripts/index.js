"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("../css/main.css");
var signalR = require("@aspnet/signalr");
var divMessages = document.querySelector("#divMessages");
var tbMessage = document.querySelector("#tbMessage");
var btnSend = document.querySelector("#btnSend");
var username = new Date().getTime();
var connection = new signalR.HubConnectionBuilder()
    .withUrl("/api/chat")
    .build();
connection.start().catch(function (err) { return document.write(err); });
connection.on("messageReceived", function (username, message) {
    var m = document.createElement("div");
    m.innerHTML = "<div class=\"message-author\">" + username + "</div><div>" + message + "</div>";
    divMessages.appendChild(m);
    divMessages.scrollTop = divMessages.scrollHeight;
});
if (tbMessage) {
    tbMessage.addEventListener("keyup", function (e) {
        if (e.keyCode === 13) {
            send();
        }
    });
}
if (btnSend) {
    btnSend.addEventListener("click", send);
}
function send() {
    connection.send("newMessage", username, tbMessage.value)
        .then(function () { return tbMessage.value = ""; });
}
var contractHub = new signalR.HubConnectionBuilder()
    .withUrl("/api/chat")
    .build();
contractHub.start().catch(function (err) { return document.write(err); });
contractHub.on("actionStarted", function (username, message) {
    console.log("actionStarted");
    var m = document.createElement("div");
    m.innerHTML = "<div class=\"action-holder\">" + username + "</div><div>" + message + "</div>";
    document.querySelector("#action-holder").remove();
    document.querySelector("#actions").append(m);
});
var checkDiscountButton = document.querySelector("#checkDiscount");
console.log(checkDiscountButton);
if (checkDiscountButton) {
    console.log("click");
    checkDiscountButton.addEventListener("click", checkDiscount);
}
function checkDiscount() {
    console.log("checkDiscount");
    contractHub.send("newMessage", username, tbMessage.value)
        .then(function () { return tbMessage.value = ""; });
}
