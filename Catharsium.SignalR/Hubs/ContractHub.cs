using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace Catharsium.SignalR.Hubs
{
    public class ContractHub : Hub
    {
        public async Task Action(string username, string message)
        {
            await this.Clients.All.SendAsync("actionStarted", username, message);
        }
    }
}