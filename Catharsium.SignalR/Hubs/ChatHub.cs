using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace Catharsium.SignalR.Hubs
{
    public class ChatHub : Hub
    {
        public async Task NewMessage(string username, string message)
        {
            await this.Clients.All.SendAsync("messageReceived", username, message);
        }
    }
}