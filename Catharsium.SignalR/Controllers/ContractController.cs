﻿using System;
using Catharsium.SignalR.Models;
using Microsoft.AspNetCore.Mvc;

namespace Catharsium.SignalR.Controllers
{
    [Route("[controller]")]
    public class Contract : Controller
    {
        [Route("")]
        [HttpGet]
        public ViewResult Index(Guid id)
        {
            return this.View(new ContractModel {CompanyName = "Con Artists Inc", EhLevel = "30"});
        }
    }
}