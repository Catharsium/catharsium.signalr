﻿namespace Catharsium.SignalR.Models
{
    public class ContractModel
    {
        public string CompanyName { get; set; }
        public string EhLevel { get; set; }
    }
}